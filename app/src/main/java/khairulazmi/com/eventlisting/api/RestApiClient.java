package khairulazmi.com.eventlisting.api;

import android.app.usage.UsageEvents;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import khairulazmi.com.eventlisting.model.Event;

/**
 * Created by Pisyek on 18/10/2016.
 * Email: pisyek@gmail.com
 * www.pisyek.com
 */

public class RestApiClient {
    private static RestApiClient sSharedInstance;
    private RequestQueue mRequestQueue;

    private RestApiClient(Context context){
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized RestApiClient sharedInstance(Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestApiClient(context);
        }
        return sSharedInstance;
    }

    public interface OnGetEventsIsCompletedListener {
        void onComplete(@Nullable List<Event> events, @Nullable VolleyError error);
    }

    public void getEvents(final OnGetEventsIsCompletedListener completedListener) {
        String url = "https://api.backendless.com/v1/data/events";
        JsonObjectRequest request = new BackendlessJsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                List<Event> events = new ArrayList<>();

                JSONArray dataArray = response.optJSONArray("data");
                for (int i=0; i< dataArray.length(); i++) {
                    JSONObject dataJson = dataArray.optJSONObject(i);
                    Event event = null;
                    try {
                        event = new Event(dataJson);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    events.add(event);
                }
                completedListener.onComplete(events, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completedListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

}
