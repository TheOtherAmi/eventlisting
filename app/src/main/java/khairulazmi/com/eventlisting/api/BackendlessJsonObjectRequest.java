package khairulazmi.com.eventlisting.api;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pisyek on 19/10/2016.
 * Email: pisyek@gmail.com
 * www.pisyek.com
 */

public class BackendlessJsonObjectRequest extends JsonObjectRequest {
    public BackendlessJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public BackendlessJsonObjectRequest(String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>(super.getHeaders());
        headers.put("application-id", "73BF43A1-564F-5AD5-FF7B-8BF226322300");
        headers.put("secret-key", "84640A75-37AF-1AD2-FF70-9C43D1716700");
        headers.put("application-type", "REST");
        return headers;
    }
}
