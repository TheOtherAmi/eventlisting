package khairulazmi.com.eventlisting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.ui.CategoryListFragment;

/**
 * Created by User on 8/10/2016.
 */

public class CreateEventActivity extends AppCompatActivity {

    private Button create_event_btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_create);

        create_event_btn = (Button)findViewById(R.id.fragment_event_create_date_btn_create_event);

        create_event_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        finish();
            }
        });
    }
}
