package khairulazmi.com.eventlisting.model;

/**
 * Created by Pisyek on 05/10/2016.
 * Email: pisyek@gmail.com
 * www.pisyek.com
 */

public class Category {
    private String mName;
    private String mThumnailUrl;

    public Category(String name, String thumnailUrl) {
        mName = name;
        mThumnailUrl = thumnailUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getThumnailUrl() {
        return mThumnailUrl;
    }

    public void setThumnailUrl(String thumnailUrl) {
        mThumnailUrl = thumnailUrl;
    }
}
