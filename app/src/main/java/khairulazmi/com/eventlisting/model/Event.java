package khairulazmi.com.eventlisting.model;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Pisyek on 05/10/2016.
 * Email: pisyek@gmail.com
 * www.pisyek.com
 */

public class Event {
    private String mName;
    private String mThumnailUrl;
    private String mDescription;
    private String mDate;
    private String mVenue;
    private Float mPrice;

    public Event(String name, String venue, String date) {
        mName = name;
        mVenue = venue;
        mDate = date;
    }

    public Event(@NonNull JSONObject jsonObject) throws Exception {
        mName = jsonObject.getString("title");
        mDescription = jsonObject.getString("description");
        mVenue = jsonObject.getString("venue");
    }

    /*(String name, String thumnailUrl, String description, Date date, String venue, Float price) {
        mName = name;
        mThumnailUrl = thumnailUrl;
        mDescription = description;
        mDate = date;
        mVenue = venue;
        mPrice = price;
    }*/

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getThumnailUrl() {
        return mThumnailUrl;
    }

    public void setThumnailUrl(String thumnailUrl) {
        mThumnailUrl = thumnailUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }


    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getVenue() {
        return mVenue;
    }

    public void setVenue(String venue) {
        mVenue = venue;
    }

    public Float getPrice() {
        return mPrice;
    }

    public void setPrice(Float price) {
        mPrice = price;
    }
}
