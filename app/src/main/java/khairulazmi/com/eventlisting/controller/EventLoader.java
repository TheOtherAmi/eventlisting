package khairulazmi.com.eventlisting.controller;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.model.Event;

/**
 * Created by User on 6/10/2016.
 */

public class EventLoader {

        private static final String TAG = "JournalLoader";

        public static List<Event> loadSamples(Context context) {
            ArrayList<Event> sampleJournals = new ArrayList<>();

            InputStream inputStream = context.getResources().openRawResource(R.raw.events);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] tokens = line.split(";");

                    String dateString = tokens[0];
                    String name = tokens[1];
                    String venue = tokens[2];

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar creationDate = Calendar.getInstance();
                    creationDate.setTime(dateFormat.parse(dateString));

                    Event journal = new Event(name, venue, dateString);
                    sampleJournals.add(journal);
                }

            } catch (Exception exception) {

                Log.e(TAG, "Load exception: " + exception.toString());
            }

            return sampleJournals;
        }




}
