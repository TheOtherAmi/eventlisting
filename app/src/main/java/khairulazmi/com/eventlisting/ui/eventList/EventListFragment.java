package khairulazmi.com.eventlisting.ui.eventList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.List;

import khairulazmi.com.eventlisting.CreateEventActivity;
import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.api.RestApiClient;
import khairulazmi.com.eventlisting.controller.EventLoader;
import khairulazmi.com.eventlisting.model.Event;

/**
 * Created by khairul-azmi on 06/09/2016.
 */
public class EventListFragment extends Fragment implements EventListHolder.onSelectEventListener {

    ImageView imageView_1,imageView_2;
    private FloatingActionButton mFloatingActionButtonCreateEvent;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EventListAdapter mEventListAdapter;
    RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_list,container,false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_swiperefresh);

        mFloatingActionButtonCreateEvent = (FloatingActionButton)rootView.findViewById(R.id.fragment_event_list_fab_create_event);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_event_list_recyclerviews);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);

        mRecyclerView.setLayoutManager(layoutManager);

        EventListAdapter eventListAdapter = new EventListAdapter(this);
        eventListAdapter.setEventList(EventLoader.loadSamples(getContext()));
        mRecyclerView.setAdapter(eventListAdapter);

        mFloatingActionButtonCreateEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CreateEventActivity.class);
                startActivityForResult(intent,100);

                Toast.makeText(getActivity(),"Create event using FAB",Toast.LENGTH_SHORT).show();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RestApiClient.sharedInstance(getContext())
                        .getEvents(new RestApiClient.OnGetEventsIsCompletedListener(){

                            @Override
                            public void onComplete(@Nullable List<Event> events, @Nullable VolleyError error) {
                                mSwipeRefreshLayout.setRefreshing(false);

                                if (events != null) {
                                    //mEventListAdapter.setEventList(events);
                                    //mEventListAdapter.notifyDataSetChanged();
                                } else {
                                    Snackbar.make(getView(), "Error: " + error.toString(), Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });




        // I comment for next modification
        /*imageView_1 = (ImageView)rootView.findViewById(R.id.fragment_event_list_image_view);
        imageView_2 = (ImageView)rootView.findViewById(R.id.fragment_event_list_image_view_2);

        imageView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout, new EventDetailFragment())
                        .addToBackStack("addbacktostacks")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }


        });
        imageView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout, new EventDetailFragment())
                        .addToBackStack("addbacktostack")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        });*/
        return rootView;
    }

    @Override
    public void onEventSelected() {
        Snackbar.make(getView(),"Event Selected",Snackbar.LENGTH_SHORT).show();
    }
}
