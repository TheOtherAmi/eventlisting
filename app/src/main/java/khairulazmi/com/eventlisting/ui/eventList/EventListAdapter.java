package khairulazmi.com.eventlisting.ui.eventList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;

import java.util.ArrayList;
import java.util.List;

import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.model.Event;

/**
 * Created by User on 6/10/2016.
 */

public class EventListAdapter extends RecyclerView.Adapter {
    private static final int VIEWTYPE_HEADER = 0;
    private static final int VIEWTYPE_CONTENT = 1;

    private EventListHolder.onSelectEventListener mListener;
    private List<Event> mEventList = new ArrayList<>();

    public List<Event> getEventList() {
        return mEventList;
    }

    public EventListAdapter(EventListHolder.onSelectEventListener listener) {
        mListener = listener;
    }

    public void setEventList(List<Event> eventList) {
        mEventList.clear();
        mEventList.addAll(eventList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.row_event_list,parent,false);
        return new EventListHolder(itemView,mListener);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //casting viewholder to EventViewHolder.
        EventListHolder eventListHolder = (EventListHolder)holder;
        Event event = mEventList.get(position);

        eventListHolder.setEvent(event);
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }


}
