package khairulazmi.com.eventlisting.ui.eventList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.model.Event;

/**
 * Created by User on 6/10/2016.
 */

public class EventListHolder extends RecyclerView.ViewHolder {

    private TextView mNameTextView,mVenueTextView,mDateTextView;
    private Event mEvent;

    public interface onSelectEventListener{
        void onEventSelected();
    }

    public EventListHolder(View itemView, final onSelectEventListener listener) {
        super(itemView);
        mNameTextView = (TextView)itemView.findViewById(R.id.row_event_list_name);
        mVenueTextView = (TextView)itemView.findViewById(R.id.row_event_list_venue);
        mDateTextView = (TextView)itemView.findViewById(R.id.row_event_list_date);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onEventSelected();
            }
        });
    }



    public Event getEvent() {
        return mEvent;
    }

    public void setEvent(Event event) {
        mEvent = event;

        updateDisplay();
    }
    private void updateDisplay(){

        mNameTextView.setText(mEvent.getName());
        mVenueTextView.setText(mEvent.getVenue());
        mDateTextView.setText(mEvent.getDate());

    }


}
