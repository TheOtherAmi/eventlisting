package khairulazmi.com.eventlisting.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import khairulazmi.com.eventlisting.CreateEventActivity;
import khairulazmi.com.eventlisting.R;
import khairulazmi.com.eventlisting.ui.eventList.EventListFragment;

/**
 * Created by khairul-azmi on 06/09/2016.
 */
public class CategoryListFragment extends Fragment implements View.OnClickListener {

    private ImageView football_img_view,basketball_img_view,tennis_img_view,rugby_img_view;
    private FloatingActionButton mFloatingActionButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_list,container,false);



        football_img_view = (ImageView) rootView.findViewById(R.id.fragment_category_list_football_img_view);
        basketball_img_view = (ImageView) rootView.findViewById(R.id.fragment_category_list_baskelball_img_view);
        tennis_img_view = (ImageView) rootView.findViewById(R.id.fragment_category_list_tennis_img_view);
        rugby_img_view = (ImageView) rootView.findViewById(R.id.fragment_category_list_rugby_img_view);
        mFloatingActionButton = (FloatingActionButton)rootView.findViewById(R.id.fab);

        football_img_view.setOnClickListener(this);
        basketball_img_view.setOnClickListener(this);
        tennis_img_view.setOnClickListener(this);
        rugby_img_view.setOnClickListener(this);
        mFloatingActionButton.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){



            case R.id.fragment_category_list_football_img_view:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout,new EventListFragment())
                        .addToBackStack("addbacktostack")
                        .commit();

                break;
            case R.id.fragment_category_list_baskelball_img_view:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout,new EventListFragment())
                        .addToBackStack("addbacktostack")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                break;
            case R.id.fragment_category_list_tennis_img_view:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout,new EventListFragment())
                        .addToBackStack("addbacktostack")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                break;
            case R.id.fragment_category_list_rugby_img_view:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_frame_layout,new EventListFragment())
                        .addToBackStack("addbacktostack")
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                break;
            case R.id.fab:
                Intent intent = new Intent(getContext(), CreateEventActivity.class);
                startActivityForResult(intent,100);

                Toast.makeText(getActivity(),"Create event using FAB",Toast.LENGTH_SHORT).show();
        }
    }
}
