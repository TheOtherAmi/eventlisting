package khairulazmi.com.eventlisting.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import khairulazmi.com.eventlisting.R;

/**
 * Created by khairul-azmi on 06/09/2016.
 */
public class EventDetailFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_detail,container,false);

        return rootView;
    }
}
